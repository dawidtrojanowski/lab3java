public class Calculations {
    public static Point2D positionGeometricCenter(Point2D[] point) {
        double sumX = 0;
        double sumY = 0;
        for(int i = 0; i < point.length; i++) {
            sumX += point[i].getX();
            sumY += point[i].getY();
        }

        return new Point2D(sumX / point.length, sumY / point.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double x = 0;
        double y = 0;
        double mass = 0;
        for(int i = 0; i < materialPoint.length; i++) {
            x += materialPoint[i].getX() * materialPoint[i].getMass();
            y += materialPoint[i].getY() * materialPoint[i].getMass();
            mass += materialPoint[i].getMass();
        }

        return new MaterialPoint2D(x / mass, y / mass, mass);
    }
}
